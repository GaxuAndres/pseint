//Materia: ESTRUCTURAS DE DATOS EST�TICOS-B1IA
//Docente: JHON JAIME OROZCO ARIAS

//Participantes:
//	ANA MARIA DAVID AGUDELO
//	ANDERSSON ANTONIO OLIVO CANTILLO
//	JORGE ANDRES CARMONA HERNANDEZ 
//	JONATHAN SMIT SALDARRIAGA ORTEGA

// Elabore un programa en PseInt que genere aleatoriamente una lista (n) de n�meros naturales
// comprendidos entre 20 y 100 y se desea saber si entre esos n�meros se encuentra un n�mero
// digitado por teclado. Si el n�mero es encontrado visualizar su posici�n en la lista.
// Desarrollar el programa por b�squeda secuencial y por b�squeda binaria utilizando un men�.

Algoritmo ejercicioTres
	
	Definir tama�o, i, opcion, n Como Entero
	Definir p_inicial, p_final, p_media como entero
	Definir encontrado Como Logico
	Definir otraBusqueda Como Caracter
	
	encontrado <- Falso
	
	// Definimos un tama�o para nuestra lista
	Escribir "Dimensi�n de la lista:"
	Leer tama�o
	
	// Definimos la lista
	Dimension lista[tama�o]
	
	// Rellenamos la lista
	Para i = 1 Hasta tama�o Hacer
		lista[i] <- Aleatorio(20, 100)
	FinPara
	
	Escribir "//*** Lista ***//"
	ImprimirLista(lista, tama�o)
	
	Escribir ""
	Escribir "Menu de opciones para busqueda de n�mero"
	Escribir "1. Busqueda Secuencial"
	Escribir "2. Busqueda Binaria"
	Escribir "3. Salir"
	Escribir "Por favor selecciona una opci�n"
	Leer opcion
	
	Repetir
		Segun opcion Hacer
			1:
				Escribir ""
				Escribir "///*****************************//"
				Escribir "///**** BUSQUEDA SECUENCIAL ****//"
				Escribir "///*****************************//"
				Escribir ""
				Escribir "Digita un n�mero:"
				Leer n
				
				// Buscamos secuenciamente el numero en la lista
				encontrado <- Falso
				i <- 0
				Mientras encontrado = Falso Y i < tama�o  Hacer
					i <- i + 1
					// Cuando se cumple la condici�n asiganmos encontrado como
					// verdadero para que finalice el ciclo "Mientras"
					Si n = lista[i] Entonces
						encontrado <- Verdadero
					FinSi
				FinMientras
				
				Si encontrado = Verdadero Entonces
					Escribir "El valor digitado " n " se encuentra en la posici�n " i
				SiNo
					Escribir "El valor no se encuentra en la lista"
				FinSi
				
				Escribir ""
				Escribir "Desea hacer otra b�squeda? (s/n)"
				Leer otraBusqueda
				
				Si otraBusqueda = "s" Entonces
					Escribir "1. Busqueda Secuencial"
					Escribir "2. Busqueda Binaria"
					Escribir "3. Salir"
					Escribir "Por favor selecciona una opci�n"
					
					Leer opcion
				SiNo
					opcion <- 3
				FinSi
			2:
				Escribir ""
				Escribir "///*****************************//"
				Escribir "///***** BUSQUEDA BINARIA ******//"
				Escribir "///*****************************//"
				Escribir ""
				
				// Ordenamos lista
				Para i<-1 Hasta tama�o Con Paso 1 Hacer
					menor <- lista[i]
					Para j <- 1 Hasta tama�o Con Paso 1 Hacer
						Si lista[j] > menor Entonces
							aux <- lista[j]
							lista[j] <- menor
							lista[i] <- aux
							menor <- aux
						FinSi
					FinPara
				FinPara
				
				Escribir "//*** Lista Ordenada ***//"
				ImprimirLista(lista, tama�o)
				
			Escribir "Digita un n�mero:"
				Leer n
				
				// valores iniciales
				encontrado <- Falso
				p_inicial <- 1
				p_final <- tama�o
				p_encontrada <- 0
				
				Mientras (encontrado = falso) Y (p_final >= p_inicial) Hacer
					// Hallamos posici�n media de la lista
					p_media <- trunc((p_inicial + p_final) / 2)
					// A continuaci�n verificamos si el valor buscado
					// se encuentra en la posicion media
					Si lista[p_media] = n Entonces
						encontrado <- Verdadero
						// guardamos la posici�n en la que se encuentra el valor
						p_encontrada <- p_media
					SiNo
						Si lista[p_media] < n Entonces
							p_inicial <- p_media + 1
						SiNo
							p_final <- p_media - 1
						FinSi
					FinSi
				FinMientras
				
				Si encontrado = Verdadero Entonces
					Escribir "El valor digitado: " n " se encuentra en la posici�n " p_encontrada
				SiNo
					Escribir "El valor no se encuentra en la lista"
				FinSi
				
				Escribir ""
				Escribir "Desea hacer otra b�squeda? (s/n)"
				Leer otraBusqueda
				
				Si otraBusqueda = "s" Entonces
					Escribir "1. Busqueda Secuencial"
					Escribir "2. Busqueda Binaria"
					Escribir "3. Salir"
					Escribir "Por favor selecciona una opci�n"
					
					Leer opcion
				SiNo
					opcion <- 3
				FinSi
		FinSegun
	Hasta Que opcion = 3
FinAlgoritmo


SubAlgoritmo  ImprimirLista (lista, tama�o)
	Para i <- 1 Hasta tama�o Con Paso 1 Hacer
		Escribir Sin Saltar lista[i] " "
	FinPara
	Escribir ""
	Escribir ""
FinSubAlgoritmo

