//Materia: ESTRUCTURAS DE DATOS ESTÁTICOS-B1IA
//Docente: JHON JAIME OROZCO ARIAS

//Participantes:
//	ANA MARIA DAVID AGUDELO
//	ANDERSSON ANTONIO OLIVO CANTILLO
//	JORGE ANDRES CARMONA HERNANDEZ 
//	JONATHAN SMIT SALDARRIAGA ORTEGA

// Elabore un programa en PseInt que genere dos listas de números naturales
// (comprendidos entre 10 y 60), L1 y L2 de 6 y 12 elementos respectivamente. Se debe:
//	a. Imprimir las listas generadas originalmente
//	b. Ordenar descendentemente cada una de las listas L1 y L2 e imprimirlas
//	c. Crear una lista L3 por intercalación o mezcla de las listas L1 y L2.
//     E imprimirla (debe quedar ordenada descendentemente)

Algoritmo programaUno
	// Definimos variables de tipo entero
	Definir i, aux, j, k Como Entero
	
	// Definimos listas
	Dimension L1[6] // lista L1
	Dimension L2[12] // Lista L2
	Dimension L3[6+12] // Lista L3 (mezcla de vector L1 y L2)
	
	// LLenamos lista L1
	Para i <- 1 Hasta 6 Con Paso 1 Hacer
		L1[i] <- Aleatorio(10, 60) // Asignamos valores aleatorios compredidos entre 10 y 60
	FinPara
	
	// LLenamos lista L2
	Para i <- 1 Hasta 12 Con Paso 1 Hacer
		L2[i] <- Aleatorio(10, 60) // Asignamos valores aleatorios compredidos entre 10 y 60
	FinPara
	
	// Escribimos lista L1
	Escribir "//*** lista L1 ***//"
	Para i <- 1 Hasta 6 Con Paso 1 Hacer
		Escribir Sin Saltar L1[i] " "
	FinPara
	Escribir ""
	Escribir ""
	
	// Escribimos lista L2
	Escribir "//*** lista L2 ***//"
	Para i <- 1 Hasta 12 Con Paso 1 Hacer
		Escribir Sin Saltar L2[i] " "
	FinPara
	Escribir ""
	Escribir ""
	
	//Ordenamos lista L1
	Para i <- 1 Hasta 6 Con Paso 1 Hacer
		menor <- L1[i]
		Para j <- 1 Hasta 6 Con Paso 1 Hacer
			Si L1[j] < menor Entonces
				aux <- L1[j]
				L1[j] <- menor
				L1[i] <- aux
				menor <- aux
			FinSi
		FinPara
	FinPara
	
	//Ordenamos lista L2
	Para i <- 1 Hasta 12 Con Paso 1 Hacer
		menor <- L2[i]
		Para j <- 1 Hasta 12 Con Paso 1 Hacer
			Si L2[j] < menor Entonces
				aux <- L2[j]
				L2[j] <- menor
				L2[i] <- aux
				menor <- aux
			FinSi
		FinPara
	FinPara
	
	// Escribimos lista L1 ordenada
	Escribir "//*** lista L1 Ordenada ***//"
	Para i <- 1 Hasta 6 Con Paso 1 Hacer
		Escribir Sin Saltar L1[i] " "
	FinPara
	Escribir ""
	Escribir ""
	
	// Escribimos lista L2 ordenada
	Escribir "//*** lista L2 Ordenada ***//"
	Para i <- 1 Hasta 12 Con Paso 1 Hacer
		Escribir Sin Saltar L2[i] " "
	FinPara
	Escribir ""
	Escribir ""
	
	
	// Creación de lista L3 por mezcla de L1 y L2 por intercalación
	i <- 1
	j <- 1
	k <- 0
	Mientras i <= 6 Y j <= 12 Hacer
		k <- k + 1
		Si L1[i] > L2[j] Entonces
			L3[k] <- L1[i]
			i <- i + 1
		SiNo
			L3[k] <- L2[j]
			j <- j + 1
		FinSi
	FinMientras
	
	Mientras i <= 6 Hacer
		k <- k + 1
		L3[k] <- L1[i]
		i <- i + 1
	FinMientras
	
	Mientras j <= 12 Hacer
		k <- k + 1
		L3[k] <- L2[j]
		j <- j + 1
	FinMientras
	
	// Escribimos lista L3 ordenada por L1 y L2 intercalados
	Escribir "//*** lista L3 Intercalada y ordenada ***//"
	Para i <- 1 Hasta 18 Con Paso 1 Hacer
		Escribir Sin Saltar L3[i] " "
	FinPara
	Escribir ""
	Escribir ""
FinAlgoritmo