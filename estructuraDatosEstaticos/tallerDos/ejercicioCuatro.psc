//Materia: ESTRUCTURAS DE DATOS EST�TICOS-B1IA
//Docente: JHON JAIME OROZCO ARIAS

//Participantes:
//	ANA MARIA DAVID AGUDELO
//	ANDERSSON ANTONIO OLIVO CANTILLO
//	JORGE ANDRES CARMONA HERNANDEZ 
//	JONATHAN SMIT SALDARRIAGA ORTEGA

// Elabore un programa en Pseint que genere aleatoriamente una lista de 10 n�meros enteros impares
// comprendidos entre 1 y 100 y que la ordene ascendentemente. Se desea conocer si un n�mero introducido
// por teclado se encuentra en la lista inicial. En caso afirmativo, averiguar su posici�n y en caso negativo
// se debe insertar este n�mero en la lista de tal forma que siga ordenada e identificar en qu� posici�n qued�.
// Imprimir la lista definitiva.

Algoritmo ejercicioCuatro
	// Definimos las variables a utilizar
	Definir tama�o Como Entero // tama�o o dimensi�n base del vector
	Definir encontrado, insertado Como Logico // Operadores l�gicos que ayudar�n a terminar ciclos "Mientras"
	
	tama�o <- 10
	
	// Definimos la lista
	// Cabe mencionar que cuando no se encuentre el n�mero en el vector
	// hay que insertar un nuevo valor en alguna posici�n de este.
	Dimension lista[tama�o + 1]
	
	// Rellenamos la lista
	Para i = 1 Hasta tama�o Hacer
		// Como queremos enteros impares debemos utilizar la ecuaci�n "2n + 1"
		// donde "n" es cualquier numero entero.
		// En este caso los valores aleatorios de "n" deben estar entre [0, 49]
		lista[i] <- 2 * Aleatorio(0, 49) + 1
	FinPara
	
	// Ordenamos lista
	// Basicamente en este bloque comparamos cada uno de los valores del vector
	// con cada uno de sus elementos
	Para i<-1 Hasta tama�o Con Paso 1 Hacer
		// Inicialmente declaramos un menor "supuesto"
		menor <- lista[i]
		Para j <- 1 Hasta tama�o Con Paso 1 Hacer
			Si lista[j] < menor Entonces
				// A continuacion guardo el valor del vector de
				// la posici�n j en una variable auxiliar de formar que no se pierda su valor
				// en el intercambio con el valor de la posici�n en i
				aux <- lista[j]
				lista[j] <- menor
				lista[i] <- aux
				menor <- aux
			FinSi
		FinPara
	FinPara
	
	Escribir ""
	Escribir "//***** Lista Inicial *****//"
	ImprimirLista(lista, tama�o)
	
	// Leemos numero a buscar
	Escribir ""
	Escribir "Introduzca un n�mero a buscar en la lista: "
	Leer n
	
	// Buscamos secuenciamente el numero en la lista
	i <- 0
	Mientras encontrado = Falso Y i < tama�o  Hacer
		i <- i + 1
		Si n = lista[i] Entonces
			// Cuando se cumple la condici�n asiganmos encontrado como
			// verdader para que finalice el ciclo "Mientras"
			encontrado <- Verdadero
		FinSi
	FinMientras
	
	Si encontrado = Verdadero Entonces
		Escribir "El valor digitado " n " se encuentra en la posici�n " i
	SiNo
		Escribir "El valor no se encuentra en la lista"
		
		i <- 0
		lista[tama�o + 1] <- 0
		Mientras insertado = Falso Y i <= tama�o
			i <- i + 1
			Si lista[i] < n Entonces
				Escribir "se debe insertar en la posici�n " i
				Para  k <- tama�o Hasta i Con Paso -1 Hacer
					lista[k+1] <- lista[k]
				FinPara
				lista[i] <- n
				insertado <- Verdadero
			FinSi
		FinMientras
	FinSi
	Escribir ""
	
	Si insertado = Verdadero Entonces
		Escribir "//***** Lista Definitiva *****//"
		ImprimirLista(lista, tama�o + 1)
	FinSi
FinAlgoritmo

// SubAlgoritmo encargado de imprimir la lista
SubAlgoritmo  ImprimirLista (lista, tama�o)
	Para i <- 1 Hasta tama�o Con Paso 1 Hacer
		Escribir Sin Saltar lista[i] " "
	FinPara
	Escribir ""
	Escribir ""
FinSubAlgoritmo