//Materia: ESTRUCTURAS DE DATOS EST�TICOS-B1IA
//Docente: JHON JAIME OROZCO ARIAS

//Participantes:
//	ANA MARIA DAVID AGUDELO
//	ANDERSSON ANTONIO OLIVO CANTILLO
//	JORGE ANDRES CARMONA HERNANDEZ 
//	JONATHAN SMIT SALDARRIAGA ORTEGA

// Una forma de ordenaci�n muy simple, pero NO muy eficaz, de elementos x1, x2,.......xn 
// en orden ascendente es esta:
// Etapa1: Identificar el elemento m�s peque�o entre x1 y xn y cambiarlo con x1.
// Etapa2: Identificar el elemento m�s peque�o entre x2 y xn y cambiarlo con x2.
// En la �ltima etapa los dos �ltimos elementos se comparan e intercambian, si es necesario, 
// y la ordenaci�n se termina. Escribir un programa en PseInt para ordenar una lista de 
// (n) n�meros naturales siguiendo este m�todo. Probar el algoritmo con los siguientes 
// vectores de 5 elementos:
// 5 4 3 2 1
// 4 5 3 1 2
// 3 1 5 2 4
// 2 4 3 5 1
// 4 1 3 2 5

//�A qu� conclusi�n se llega?
// A pesar de que en la comparaci�n principal del algortimo se busca un numero menor
// ademas de ser NO eficaz , No ordena de manera adecuada.
// El algoritmo s�lo ordena en casoso muy especificos.
// La propuesta de algoritmo le fata mas trabajo, pues siempre se va a obtener el numero
// mayor en la �ltima posici�n del vector.

Algoritmo ejercicioDos
	// definimos variables
	Definir i, n, opcion Como Entero
	
	Escribir "1. Creaci�n y ordenado de vector"
	Escribir "2. Vectores de prueba"
	Escribir "Por favor selecciona una opci�n"
	Leer opcion
	
	Segun opcion Hacer
		1:
			Escribir ""
			Escribir "///***********************************************//"
			Escribir "///******** CREACI�N Y ORDENADO DE VECTOR ********//"
			Escribir "///***********************************************//"
			Escribir ""
			Escribir "Digita tama�o de vector:"
			Leer n
			Dimension lista[n]
			
			Para i <- 1 Hasta n Con Paso 1 Hacer
				Escribir Sin Saltar "Digite elemento ", i, " del vector: "
				Leer lista[i]
			FinPara
			
			Ordenar(lista, n)
			Escribir ""
		2:
			Escribir ""
			Escribir "///************************************//"
			Escribir "///******** VECTORES DE PRUEBA ********//"
			Escribir "///************************************//"
			Escribir ""
			
			// Vectores de prueba
			Dimension v1[5]
			Dimension v2[5]
			Dimension v3[5]
			Dimension v4[5]
			Dimension v5[5]
			
			// Relleno de vectores de prueba
			RellenarLista(v1, 5, 4, 3, 2, 1)
			RellenarLista(v2, 4, 5, 3, 1, 2)
			RellenarLista(v3, 3, 1, 5, 2, 4)
			RellenarLista(v4, 2, 4, 3, 5, 1)
			RellenarLista(v5, 4, 1, 3, 2, 5)
			
			Escribir "1. Prueba de lista 1 = [5, 4, 3, 2, 1]"
			Ordenar(v1, 5)
			Escribir ""
			
			Escribir "2. Prueba de lista 2 = [4, 5, 3, 1, 2]"
			Ordenar(v2, 5)
			Escribir ""
			
			Escribir "3. Prueba de lista 3 = [3, 1, 5, 2, 4]"
			Ordenar(v3, 5)
			Escribir ""
			
			Escribir "4. Prueba de lista 4 = [2, 4, 3, 5, 1]"
			Ordenar(v4, 5)
			Escribir ""
			
			Escribir "5. Prueba de lista 5 = [4, 1, 3, 2, 5]"
			Ordenar(v5, 5)
			Escribir ""
	FinSegun
FinAlgoritmo


// Ordenado propuesto por el ejercicio
SubAlgoritmo  Ordenar (lista por referencia, tama�o)
	Para i <- 1 Hasta tama�o Con Paso 1 Hacer
		// Si la siguiente condici�n se cumple ocurre el intercambio
		Si lista[i] > lista[tama�o] Entonces
			aux <- lista[i]
			lista[i] <- lista[tama�o]
			lista[tama�o] <- aux
		FinSi
		// Imprimimos lista por paso
		Escribir Sin Saltar "   Paso " i " => "
		ImprimirLista(lista, tama�o)
	FinPara
FinSubAlgoritmo

SubAlgoritmo RellenarLista(lista Por Referencia, valor_1, valor_2, valor_3, valor_4, valor_5)
	lista[1] <- valor_1
	lista[2] <- valor_2
	lista[3] <- valor_3
	lista[4] <- valor_4
	lista[5] <- valor_5
FinSubAlgoritmo

SubAlgoritmo  ImprimirLista (lista, tama�o)
	Para i <- 1 Hasta tama�o Con Paso 1 Hacer
		Escribir Sin Saltar lista[i] " "
	FinPara
	Escribir ""
FinSubAlgoritmo
	